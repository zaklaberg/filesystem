# An application to deeply investigate the file system

## You can...
- List files in directory(and filter by extension)
- Search for words in files
- Count occurence of words in files
- Get file size
- Get lines in text files

... and other gems of _extreme_ utility. Have ye fun.

## Usage
To use this glorious program, build and copy the resources folder into the corresponding .exe directory. After that, the possibilities are limited.
