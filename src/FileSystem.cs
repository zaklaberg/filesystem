﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace filesystem
{
    public class FileSystem
    {
        static void Main(string[] args)
        {
            var directoryService = new DirectoryService("resources");
            var fileService = new FileService("resources/dracula.txt");

            Action<string> listFiles = ext =>
            {
                Console.Clear();
                string[] files = directoryService.GetFiles(ext);
                foreach(string file in files)
                {
                    Console.WriteLine(file);
                }
            };

            Action listFilesByExt = () =>
            {
                string[] extensions = directoryService.GetExtensionsInDirectory();

                List<(string, Action)> options = new List<(string, Action)>();
                
                foreach (string ext in extensions)
                {
                    options.Add((ext, () => listFiles(ext)));
                };

                OptionSelector os = new OptionSelector(options, "Choose an extension: ", true);
                os.RenderOptions();
                os.UserSelect();
            };

            Action nameOfFile = () =>
            {
                Console.Clear();
                Console.WriteLine($"The name of dracula.txt is... {fileService.GetFileName()} ");
            };

            Action sizeOfFile = () =>
            {
                Console.Clear();
                long filesize = fileService.GetFileSize();
                Console.WriteLine($"dracula.txt is {filesize} bytes long. Or {filesize/1000} kB.");
            };

            Action linesInFile = () =>
            {
                Console.Clear();
                Console.WriteLine($"dracula.txt contains {fileService.GetNumLinesInFile()} lines.");
            };

            Action wordInFile = () =>
            {
                Console.Clear();
                Console.WriteLine("Enter the word you wish to search for...");
                string word = Console.ReadLine();
                bool wordOccurs = fileService.WordOccurencesInFile(word) > 0;
                Console.WriteLine(wordOccurs ? "Well, yes, that word does occur, indeed..." : "No, obviously that word isn't here. Jeez...");
            };

            Action wordOccurencesInFile= () =>
            {
                Console.Clear();
                Console.WriteLine("Enter the word you wish to search for...");
                string word = Console.ReadLine();
                int occurences = fileService.WordOccurencesInFile(word);
                Console.WriteLine($"The word {word} occured {occurences} times.");
            };

            List<(string, Action)> options = new List<(string, Action)>
            {
                ("List all files", () => listFiles(null)),
                ("List files with extension", listFilesByExt),
                ("Get name of Dracula.txt", nameOfFile),
                ("Get size of Dracula.txt", sizeOfFile),
                ("Get number of lines in Dracula.txt", linesInFile),
                ("Search for word in Dracula.txt", wordInFile),
                ("Get occurence count of word in Dracula.txt", wordOccurencesInFile)
            };

            OptionSelector os = new OptionSelector(options);

            // Loop for user input until user quits.
            os.RenderOptions();
            os.UserSelectLoop();
        }
    }
}
