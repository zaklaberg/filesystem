﻿using System;
using System.Collections.Generic;
using System.Text;

namespace filesystem
{
    public class OptionSelector
    {
        private Dictionary<string, Action> commands = new Dictionary<string, Action>();
        private List<string> options = new List<string>();
        private int selectedOption = 0;
        private int topCursorPos = 1;
        private int bottomCursorPos = 0;
        private bool autoQuit = false;
        
        private string title = "Please select an option. Use arrow keys to navigate, enter to select.";
        private string exitString = "exit";

        public string SelectedOption { get { return this.options[this.selectedOption]; } }

        public OptionSelector(List<(string, Action)> options)
        {
            foreach (var option in options)
            {
                this.options.Add(option.Item1);
                commands.Add(option.Item1, option.Item2);
            }

            this.options.Add(this.exitString);
        }
        public OptionSelector(List<(string, Action)> options, string title) : this(options)
        {
            this.title = title;
        }

        public OptionSelector(List<(string, Action)> options, string title, bool autoQuit) : this(options, title)
        {
            this.autoQuit = autoQuit;
        }

        // Display the possible options to user
        public void RenderOptions()
        {
            ClearScreen();

            Console.WriteLine(title);
            for (int i = 0; i < options.Count; ++i)
            {
                if (i == selectedOption)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(options[i]);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else Console.WriteLine(options[i]);
            }

            bottomCursorPos = Console.CursorTop - 1;
        }

        // Highlight the selected option
        private void ChangeSelected(int newSelectedOption)
        {
            // Remove highlight from previously selected option
            Console.SetCursorPosition(0, topCursorPos + selectedOption);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(options[selectedOption]);

            // Add highlight to newly selected option
            Console.SetCursorPosition(0, topCursorPos + newSelectedOption);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(options[newSelectedOption]);
            Console.ForegroundColor = ConsoleColor.White;

            selectedOption = newSelectedOption;
        }

        public void ClearScreen()
        {
            if (bottomCursorPos != 0)
            {
                for (int i = bottomCursorPos; i > -1; --i)
                {
                    Console.SetCursorPosition(0, i);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            Console.Clear();
        }

        public void UserSelect()
        {
            Console.SetCursorPosition(0, selectedOption + 1);

            var key = Console.ReadKey();
            
            // Navigation & selection
            while ((key.Key != ConsoleKey.Enter))
            {
                if (key.Key == ConsoleKey.UpArrow)
                {
                    // navigate up
                    var newCursorPos = Math.Max(1, Console.CursorTop - 1);
                    Console.SetCursorPosition(0, newCursorPos);
                    ChangeSelected(newCursorPos - 1);
                }
                else if (key.Key == ConsoleKey.DownArrow)
                {
                    // navigate down
                    var newCursorPos = Math.Min(bottomCursorPos, Console.CursorTop + 1);
                    Console.SetCursorPosition(0, newCursorPos);
                    ChangeSelected(newCursorPos - 1);
                }

                key = Console.ReadKey();
            }

            // The last option is to quit
            if (selectedOption == options.Count - 1)
                return;

            // Execute corresponding action
            commands[options[selectedOption]]();

            // Possibly delay return - user will (likely) want to see the results of his actions
            if (!this.autoQuit)
            {
                Console.SetCursorPosition(0, Console.CursorTop + 1);
                Console.WriteLine("Press any key to return to menu...");
                Console.ReadKey();
            }
        }

        public void UserSelectLoop()
        {
            this.UserSelect();

            // If selected option was the last one, user wants to quit the loop
            Console.Clear();
            if (selectedOption == options.Count - 1)
                return;

            // Loop back
            RenderOptions();
            UserSelectLoop();
        }

    }
}
