﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace filesystem
{
    public class FileService
    {
        private string filePath = null;
        private FileInfo fileInfo = null;

        private LoggingService loggingService = new LoggingService("fileservice.log.txt");

        public FileService(string filePath)
        {
            this.filePath = filePath;
            this.fileInfo = new FileInfo(filePath);

            if (!this.fileInfo.Exists)
            {
                throw new Exception("Please provide a path to a file that actually exists.");
            }
        }

        public string GetFileName()
        {
            if (fileInfo == null) throw new Exception("Class not properly initialized.");
            loggingService.Log($"Filename was {fileInfo.Name}");
            return fileInfo.Name;
        }

        public long GetFileSize()
        {
            if (fileInfo == null) throw new Exception("Class not properly initialized.");
            loggingService.Log($"The size of {fileInfo.Name} was {fileInfo.Length / 1000} kB.");
            return fileInfo.Length;
        }

        public int GetNumLinesInFile()
        {
            if (fileInfo == null) throw new Exception("Class not properly initialized.");

            int logId = loggingService.InitLog();
            int numLines = 0;
            using (StreamReader sr = new StreamReader(filePath))
            {
                while (sr.ReadLine() != null)
                {
                    ++numLines;
                }
            }
            loggingService.EndLog($"There were {numLines} lines in {fileInfo.Name}.", logId);

            return numLines;
        }

        public int WordOccurencesInFile(string word)
        {
            if (fileInfo == null) throw new Exception("Class not properly initialized.");

            int logId = loggingService.InitLog();

            int wordCount = 0;
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    wordCount += Regex.Matches(line, @"\b" + word.Trim() + @"\b", RegexOptions.IgnoreCase).Count;
                }
            }

            loggingService.EndLog($"The word {word} occured {wordCount} times in {fileInfo.Name}.", logId);

            return wordCount;
        }
    }
}
