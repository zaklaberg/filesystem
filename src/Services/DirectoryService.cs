﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace filesystem
{
    public class DirectoryService
    {
        private string directoryPath = null;
        private LoggingService loggingService = new LoggingService("dirservice.log.txt");
        public DirectoryService(string directoryPath)
        {
            if (!Directory.Exists(directoryPath)) throw new Exception("Please provide a path to a directory that actually exists.");

            this.directoryPath = directoryPath;
        }

        public string[] GetExtensionsInDirectory()
        {
            int logId = loggingService.InitLog();
            // Get all files in dir
            string[] filenames = Directory.GetFiles(directoryPath);

            // Extract extensions
            for (int i = 0; i < filenames.Length; ++i)
            {
                filenames[i] = Path.GetExtension(filenames[i]).Substring(1);
            }

            // Pick uniques
            HashSet<string> extensions = new HashSet<string>(filenames);
            loggingService.EndLog($"Found {extensions.Count} extensions in {directoryPath}.", logId);
            return new List<string>(extensions).ToArray();
        }

        // Returns all files in directory
        // If a non-null extensionFilter is passed, only returns files with a matching extension
        public string[] GetFiles(string extensionFilter = null)
        {
            int logId = loggingService.InitLog();

            // Get all files in dir
            string[] filepaths = Directory.GetFiles(directoryPath);
            List<string> outFiles = new List<string>();

            // Filter by extension, if desired
            foreach (string filepath in filepaths)
            {
                bool extensionEqual = (extensionFilter == Path.GetExtension(filepath).Substring(1));
                if (extensionFilter != null && !extensionEqual)
                    continue;

                outFiles.Add(filepath);
            }
            string message = extensionFilter == null ? $"Found {outFiles.Count} files in {directoryPath}." : $"Found {outFiles.Count} .{extensionFilter}-files in {directoryPath}.";
            loggingService.EndLog(message, logId);

            return outFiles.ToArray();
        }
    }
}
