﻿using System;
using System.IO;
using System.Collections.Generic;

namespace filesystem
{
	class Log
    {
		public string Message { get; set; }

		// Only used for initlog/endlog. Measures execution time in milliseconds.
		public double ExecutionTime { get; set; } = -1;
		public string FunctionLogged { get; set; } = null;
		public DateTime LogTime { get; set; }
    }
	public class LoggingService
	{
		private int logId = 0;
		public string LogFilename { get; set; } = "";
		private Dictionary<int, Log> logs = new Dictionary<int, Log>();

		public LoggingService(string logFileName)
		{
			// Make sure we have a separate directory for logs
			if (!Directory.Exists("logs")) Directory.CreateDirectory("logs");

			// The file to log to, relative to the logs directory.
			LogFilename = logFileName;
		}
		
		private void WriteLog(int logId)
        {
			if (LogFilename == "") throw new Exception("Please provide a file name for the log.");
			if (!logs.ContainsKey(logId)) throw new Exception($"Log with id {logId} does not exist.");

			// Logs: [Date]: (Function) (ExecutionTime) [message],
			// where [] is always logged and () are optional.
			using(StreamWriter sw = new StreamWriter($"logs/{LogFilename}", true))
            {
				Log log = logs[logId];

				string message = $"{log.LogTime}: {log.Message} ";
				if (log.ExecutionTime != -1)
                {
					if (log.FunctionLogged != null)
						message += $"{log.FunctionLogged} ";
					else
						message += "The function ";

					message += $"took {log.ExecutionTime} ms to execute.";
                }
				sw.WriteLine(message);

				// This makes the console pretty messy, but since it's a requirement in the spec..
				Console.WriteLine(message);
			}

			logs.Remove(logId);
        }

		/// <summary>
		/// Initializes a log. Must be called pairwise with EndLog(), 
		/// which automatically writes out the time taken since InitLog() was called.
		/// </summary>
		/// 
		/// <returns>
		/// A log id. Must be passed to EndLog()
		/// </returns>
		public int InitLog()
        {
			// Several InitLogs() may be called before the first is terminated with EndLog()
			// So - we store them in a dictionary and remove when they are written to file.
			logs[++logId] = new Log();
			logs[logId].LogTime = DateTime.Now;
			return logId;
        }

		public void EndLog(string message, int logId)
        {
			if (!logs.ContainsKey(logId)) throw new Exception("Invalid log id.");

			logs[logId].Message = message;
			logs[logId].ExecutionTime = (DateTime.Now - logs[logId].LogTime).TotalMilliseconds;

			WriteLog(logId);
        }
		public void Log(string message)
		{
			logs[++logId] = new Log();
			logs[logId].Message = message;
			logs[logId].LogTime = DateTime.Now;

			WriteLog(logId);
		}
	}
}
